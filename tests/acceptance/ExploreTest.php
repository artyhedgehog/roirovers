<?php
namespace acceptance;

use AppBundle\Command\ExploreCommand;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class ExploreTest extends KernelTestCase
{
    public function testExecute()
    {
        $command = $this->prepareCommand();
        $tester = new CommandTester($command);
        $tester->execute([
            'command'  => $command->getName(),
            'filename' => __DIR__ . '/input1',
        ]);

        $output = $tester->getDisplay();
        $this->assertStringEqualsFile(__DIR__ . '/output1', $output);
    }

    public function testExecuteWithLimitations()
    {
        $command = $this->prepareCommand();
        $tester = new CommandTester($command);
        $tester->execute([
            'command' => $command->getName(),
            'filename' => __DIR__ . '/input2',
        ]);

        $output = $tester->getDisplay();
        $this->assertStringEqualsFile(__DIR__ . '/output2', $output);
    }

    /**
     * @return Command
     */
    private function prepareCommand()
    {
        self::bootKernel();
        $application = new Application(self::$kernel);

        $application->add(new ExploreCommand());

        return $application->find('explore');
    }
}
