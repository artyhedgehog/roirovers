<?php
namespace AppBundle\Model;

class CoordinatesTest extends \PHPUnit_Framework_TestCase
{
    public function testCoordinatesCreation()
    {
        $x = 13;
        $y = -15;
        $c = new Coordinates($x, $y);
        $this->assertEquals($x, $c->getX());
        $this->assertEquals($y, $c->getY());
    }
}
