<?php
namespace AppBundle\Model;


class PositionTest extends \PHPUnit_Framework_TestCase
{
    public function testToString()
    {
        $position1 = new Position(new Coordinates(10, 20), Direction::SOUTH());
        $this->assertEquals('10 20 S', $position1->__toString());

        $position2 = new Position(new Coordinates(-10, 0), Direction::WEST());
        $this->assertEquals('-10 0 W', $position2->__toString());
    }
}
