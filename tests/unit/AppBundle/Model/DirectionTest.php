<?php
namespace AppBundle\Model;

class DirectionTest extends \PHPUnit_Framework_TestCase
{
    public function testValue()
    {
        $east = Direction::get(Direction::EAST);
        $this->assertEquals(Direction::EAST, $east->getValue());

        $north = Direction::byValue(Direction::NORTH);
        $this->assertEquals(Direction::NORTH, $north->getValue());

        $south = Direction::SOUTH();
        $this->assertEquals(Direction::SOUTH, $south->getValue());

        $west = Direction::byName('WEST');
        $this->assertEquals(Direction::WEST, $west->getValue());
    }

    public function testTurns()
    {
        $north = Direction::NORTH();
        $west = $north->left();
        $this->assertTrue($west->is(Direction::WEST));

        $south = $west->left();
        $this->assertTrue($south->is(Direction::SOUTH));

        $east = $north->right();
        $this->assertTrue($east->is(Direction::EAST));
        $this->assertEquals($east, $south->left());

        $this->assertTrue($south->left()->left()->left()->is(Direction::WEST));

        $this->assertTrue($west->right()->right()->right()->left()->is(Direction::EAST));
    }
}
