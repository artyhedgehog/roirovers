<?php
namespace AppBundle\Model;

class MovementTest extends \PHPUnit_Framework_TestCase
{

    public function testMovements()
    {
        $west = Movement::west();
        $this->assertEquals(new Coordinates(-1, 0), $west->make(new Coordinates(0, 0)));

        $north = Movement::north();
        $this->assertEquals(new Coordinates(20, -15), $north->make(new Coordinates(20, -16)));

        $east = Movement::inDirection(Direction::EAST());
        $this->assertEquals(new Coordinates(5, 4), $east->make(new Coordinates(4, 4)));

        $south = Movement::inDirection(Direction::SOUTH());
        $this->assertEquals(new Coordinates(-13, 20), $south->make(new Coordinates(-13, 21)));
    }
}
