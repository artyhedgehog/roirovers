<?php
namespace AppBundle\Model;

class RoverTest extends \PHPUnit_Framework_TestCase
{

    public function testMove()
    {
        $position = new Position(new Coordinates(0, 0), Direction::NORTH());
        $plateau = new Plateau(2, 1);
        $rover = new Rover($plateau, $position);

        $rover->move();
        $this->assertEquals(
            new Coordinates(0, 1),
            $rover->getPosition()->getCoordinates(),
            'Rover must move in initial direction'
        );

        $rover->move();
        $this->assertEquals(
            new Coordinates(0, 1),
            $rover->getPosition()->getCoordinates(),
            'Rover cannot move beyond plateau limits'
        );

        $rover->turnRight();
        $rover->move();
        $this->assertEquals(
            new Coordinates(1, 1),
            $rover->getPosition()->getCoordinates(),
            'Rover must move in changed direction'
        );

        new Rover($plateau, new Position(new Coordinates(2, 1), Direction::SOUTH()));
        $rover->move();
        $this->assertEquals(
            new Coordinates(1, 1),
            $rover->getPosition()->getCoordinates(),
            'Rover cannot move if there is another rover on the way'
        );
    }
}
