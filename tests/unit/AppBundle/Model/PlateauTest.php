<?php
namespace AppBundle\Model;

class PlateauTest extends \PHPUnit_Framework_TestCase
{
    public function testIsAvailable()
    {
        $plateau = new Plateau(3, 1);

        $this->assertTrue($plateau->isAvailable(new Coordinates(0, 0)));
        $this->assertTrue($plateau->isAvailable(new Coordinates(3, 1)));
        $this->assertTrue($plateau->isAvailable(new Coordinates(1, 1)));

        $this->assertFalse($plateau->isAvailable(new Coordinates(2, -1)));
        $this->assertFalse($plateau->isAvailable(new Coordinates(4, 0)));
        $this->assertFalse($plateau->isAvailable(new Coordinates(-1, 2)));
    }

    public function testIsAvailableWithCollisions()
    {
        $plateau = new Plateau(5, 5);
        $coordinates = new Coordinates(3, 3);
        $plateau->arrive($coordinates, new Rover($plateau, new Position($coordinates, Direction::NORTH())));
        
        $this->assertFalse($plateau->isAvailable($coordinates));
    }
}
