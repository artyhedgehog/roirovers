<?php

namespace AppBundle\Exception;

use Exception;

/**
 * UnexpectedInputException class.
 */
class UnexpectedInputException extends \InvalidArgumentException
{
    /** @noinspection MoreThanThreeArgumentsInspection */
    /**
     * @param string $line
     * @param string $expected
     * @param string $found
     * @param int $code
     * @param Exception|null $previous
     * @return static
     */
    public static function with($line, $expected, $found, $code = 0, Exception $previous = null)
    {
        $message = "Wrong input in line '{$line}': expected {$expected} - got {$found}";
        return new static($message, $code, $previous);
    }
}
