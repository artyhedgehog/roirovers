<?php
namespace AppBundle\Command;

use AppBundle\Exception\UnexpectedInputException;
use AppBundle\Model\Coordinates;
use AppBundle\Model\Direction;
use AppBundle\Model\Navigator;
use AppBundle\Model\Plateau;
use AppBundle\Model\Position;
use AppBundle\Model\Rover;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExploreCommand extends ContainerAwareCommand
{
    const ARG_FILE = 'filename';

    protected function configure()
    {
        $this
            ->setName('explore')
            ->setDescription('Explore plateau with rovers given a file with orders')
            ->addArgument(self::ARG_FILE, InputArgument::REQUIRED, 'Name of file with orders');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument(self::ARG_FILE);
        $file = new \SplFileObject($filename, 'rb');
        $file->setFlags(\SplFileObject::SKIP_EMPTY | \SplFileObject::READ_AHEAD);
        $file->fscanf('%d %d', $maxX, $maxY);
        $plateau = new Plateau($maxX, $maxY);
        while (!$file->eof()) {
            $position = $this->produceRoverFinalPosition($file, $plateau);
            $output->writeln((string)$position);
            $file->next();
        }
    }

    /**
     * @param \SplFileObject $file
     * @param Plateau $plateau
     * @return Position
     */
    private function produceRoverFinalPosition(\SplFileObject $file, Plateau $plateau)
    {
        $startX = 0;
        $startY = 0;
        $startDirectionChar = Direction::NORTH;

        $startingPositionLine = $file->current();
        sscanf($startingPositionLine, '%d %d %s', $startX, $startY, $startDirectionChar);

        $startingPoint = new Coordinates($startX, $startY);
        try {
            /** @noinspection ExceptionsAnnotatingAndHandlingInspection \LogicException should never occur */
            $startingDirection = Direction::byValue($startDirectionChar);
        } catch (\InvalidArgumentException $e) {
            throw UnexpectedInputException::with($file->current(), "'N', 'S', 'W' or 'E'", $startDirectionChar);
        }
        $rover = new Rover($plateau, new Position($startingPoint, $startingDirection));

        $navigator = new Navigator($rover);

        $file->next();

        $navigator->executeOrders($file->current());
        return $rover->getPosition();
    }
}
