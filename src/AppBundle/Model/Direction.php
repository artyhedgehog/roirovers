<?php
namespace AppBundle\Model;

use MabeEnum\Enum;

/**
 * Direction on surface
 * @method static Direction NORTH()
 * @method static Direction EAST()
 * @method static Direction SOUTH()
 * @method static Direction WEST()
 */
class Direction extends Enum
{
    const NORTH = 'N';
    const EAST = 'E';
    const SOUTH = 'S';
    const WEST = 'W';

    /**
     * Get direction to the left of the current
     * @return static
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function left()
    {
        $ordinal = $this->getOrdinal() - 1;
        if ($ordinal < 0) {
            $ordinal = 3;
        }
        return Direction::byOrdinal($ordinal);
    }

    /**
     * Get direction to the right of the current
     * @return static
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function right()
    {
        $ordinal = $this->getOrdinal() + 1;
        if ($ordinal > 3) {
            $ordinal = 0;
        }
        return Direction::byOrdinal($ordinal);
    }

    /**
     * Returns value e.g. letter representing direction
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getValue();
    }
}
