<?php

namespace AppBundle\Model;

/**
 * Reads orders string and commands the rover assigned to it with the orders
 */
class Navigator
{
    const ORDER_MOVE = 'M';
    const ORDER_TURN_LEFT = 'L';
    const ORDER_TURN_RIGHT = 'R';

    /** @var Rover */
    private $rover;

    public function __construct(Rover $rover)
    {
        $this->rover = $rover;
    }

    /**
     * @param string $sequence
     */
    public function executeOrders($sequence)
    {
        foreach (str_split($sequence) as $char) {
            $this->execute($char);
        }
    }

    /**
     * @param string $char Order character
     */
    private function execute($char)
    {
        switch ($char) {
            case self::ORDER_MOVE:
                $this->rover->move();
                break;
            case self::ORDER_TURN_LEFT:
                $this->rover->turnLeft();
                break;
            case self::ORDER_TURN_RIGHT:
                $this->rover->turnRight();
                break;
        }
    }
}
