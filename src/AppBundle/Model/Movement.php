<?php

namespace AppBundle\Model;

/**
 * Movement representation
 */
class Movement
{
    /** @var int */
    private $dX;
    /** @var int */
    private $dY;


    public static function inDirection(Direction $direction)
    {
        switch ($direction->getValue()) {
            case Direction::EAST:
                return static::east();
            case Direction::NORTH:
                return static::north();
            case Direction::SOUTH:
                return static::south();
            case Direction::WEST:
                return static::west();
            default:
                return static::none();
        }
    }


    public static function north()
    {
        return new static(0, 1);
    }

    public static function east()
    {
        return new static(1, 0);
    }

    public static function south()
    {
        return new static(0, -1);
    }

    public static function west()
    {
        return new static(-1, 0);
    }

    public static function none()
    {
        return new static(0, 0);
    }


    /**
     * @param int $dX
     * @param int $dY
     */
    public function __construct($dX, $dY)
    {
        $this->dX = $dX;
        $this->dY = $dY;
    }

    /**
     * @param Coordinates $from
     * @return Coordinates
     */
    public function make(Coordinates $from)
    {
        return new Coordinates(
            $from->getX() + $this->dX,
            $from->getY() + $this->dY
        );
    }
}
