<?php

namespace AppBundle\Model;

/**
 * Coordinates class.
 */
class Coordinates
{
    /** @var int */
    private $x;
    /** @var int */
    private $y;

    /**
     * @param int $x
     * @param int $y
     */
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    public function __toString()
    {
        return sprintf('%d %d', $this->x, $this->y);
    }
}
