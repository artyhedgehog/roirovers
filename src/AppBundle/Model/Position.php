<?php

namespace AppBundle\Model;

/**
 * Position including coordinates and direction
 */
class Position
{
    /** @var Coordinates */
    private $coordinates;
    /** @var Direction */
    private $direction;

    public function __construct(Coordinates $coordinates, Direction $direction)
    {
        $this->coordinates = $coordinates;
        $this->direction = $direction;
    }

    /**
     * @return Coordinates
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @return Direction
     */
    public function getDirection()
    {
        return $this->direction;
    }

    public function __toString()
    {
        return sprintf('%s %s', $this->coordinates, $this->direction);
    }
}
