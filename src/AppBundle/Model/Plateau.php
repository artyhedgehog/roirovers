<?php
namespace AppBundle\Model;

/**
 * Plateau
 */
class Plateau
{
    /** @var int */
    private $maxX;
    /** @var int */
    private $maxY;
    /** @var int */
    private $minX;
    /** @var int */
    private $minY;
    /** @var array Objects on the plateau */
    private $objects = [];

    /**
     * @param int $maxX
     * @param int $maxY
     * @param int $minX
     * @param int $minY
     */
    public function __construct($maxX, $maxY, $minX = 0, $minY = 0)
    {
        $this->maxX = $maxX;
        $this->maxY = $maxY;
        $this->minX = $minX;
        $this->minY = $minY;
    }

    public function isAvailable(Coordinates $coordinates)
    {
        return $this->withinLimits($coordinates) && $this->isEmpty($coordinates);
    }

    public function leave(Coordinates $coordinates, $subject)
    {
        $key = $this->key($coordinates);
        if ($this->objects[$key] === $subject) {
            $this->objects[$key] = null;
        }
    }

    public function arrive(Coordinates $coordinates, $subject)
    {
        if (!$this->isAvailable($coordinates)) {
            return false;
        }
        $key = $this->key($coordinates);
        $this->objects[$key] = $subject;
        return true;
    }

    private function withinLimits(Coordinates $coordinates)
    {
        $x = $coordinates->getX();
        $y = $coordinates->getY();
        return $this->minX <= $x && $x <= $this->maxX
            && $this->minY <= $y && $y <= $this->maxY;
    }

    private function isEmpty($coordinates)
    {
        return empty($this->objects[$this->key($coordinates)]);
    }

    /**
     * @param Coordinates $coordinates
     * @return string
     */
    private function key(Coordinates $coordinates)
    {
        return (string)$coordinates;
    }
}
