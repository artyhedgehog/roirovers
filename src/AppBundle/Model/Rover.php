<?php

namespace AppBundle\Model;

/**
 * Rover model
 */
class Rover
{
    /** @var Plateau */
    private $plateau;
    /** @var Position */
    private $position;

    public function __construct(Plateau $plateau, Position $position)
    {
        $this->plateau = $plateau;
        $this->position = $position;
        $this->plateau->arrive($position->getCoordinates(), $this);
    }

    public function turnLeft()
    {
        $this->position = new Position($this->position->getCoordinates(), $this->position->getDirection()->left());
    }

    public function turnRight()
    {
        $this->position = new Position($this->position->getCoordinates(), $this->position->getDirection()->right());
    }

    public function move()
    {
        $direction = $this->position->getDirection();
        $oldCoordinates = $this->position->getCoordinates();

        $movement = Movement::inDirection($direction);
        $newCoordinates = $movement->make($oldCoordinates);

        if ($this->plateau->arrive($newCoordinates, $this)) {
            $this->plateau->leave($oldCoordinates, $this);
            $this->position = new Position($newCoordinates, $direction);
        }
    }

    public function getPosition()
    {
        return $this->position;
    }
}
